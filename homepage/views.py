from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators
from django.urls import reverse
from .models import Status
from .forms import StatusInput


# Create your views here.

def index(request):
    all_status = Status.objects.all().order_by('-time')

    if request.method == 'POST':
        status_form = StatusInput(request.POST)

        if status_form.is_valid():
            status_form.save()
            return redirect(reverse('homepage:index'))
    else:
        status_form = StatusInput()

    context = {
        'all_status': all_status,
        'status_form': status_form
    }

    return render(request, 'homepage.html', context)